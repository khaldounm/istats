/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {
  onSnapshot,
  collection,
  query,
  where,
} from 'firebase/firestore';
import { db } from '../../firebase';
import { useAuth } from '../../contexts/AuthContext';
import { Divider, Paper } from '@mui/material';
import EventRepeatIcon from '@mui/icons-material/EventRepeat';

const numberFormat = (value) =>
  new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency: 'GBP'
  }).format(value);

const Details = () => {
  const { currentUser } = useAuth();
  const [assetType, setAssetType] = React.useState(null);
  const [asset, setAsset] = React.useState(null);
  const [data, setData] = React.useState([]);
  const [bought, setBought] = React.useState(0);
  const [totalValue, setTotalValue] = React.useState(0);
  const [total, setTotal] = React.useState(0);
  const [quantity, setQuantity] = React.useState(0);
  const collectionRef = collection(db, 'investments');

  const handleChange = (e) => {
    setTotal(0);
    setTotalValue(0);
    setQuantity(0);
    setBought(0);
    setAsset(e.target.value);
  }

  const handleTypeChange = (e) => {
    setAssetType(e.target.value);
    setAsset(null);
    setTotal(0);
    setTotalValue(0);
    setQuantity(0);
    setBought(0);
  }

  useEffect(() => {
    let q = query(
      collectionRef,
      where('userId', '==', currentUser.uid));
    if (asset && !assetType) {
      q = query(
        collectionRef,
        where('userId', '==', currentUser.uid),
        where('iso3', '==', asset.toUpperCase()),
      );
    }
    if (assetType && !asset) {
      q = query(
        collectionRef,
        where('userId', '==', currentUser.uid),
        where('type', '==', assetType.toLowerCase()),
      );
    }
    if (assetType && asset) {
      q = query(
        collectionRef,
        where('userId', '==', currentUser.uid),
        where('type', '==', assetType.toLowerCase()),
        where('iso3', '==', asset.toUpperCase()),
      );
    }
    const readStream = onSnapshot(q, (querySnapshot) => {
      const list = [];
      const sortedList = [];
      querySnapshot.forEach((doc) => {
        list.push({ id: doc.id, purchaseDateIso: doc.data().purchaseDate.toDate().toDateString(), ...doc.data() });
      });
      list.forEach((item, i) => {
        if (i === 0 || !sortedList.find(sortedItem => sortedItem.iso3 === item.iso3)) {
          sortedList.push(item)
        }
      });
      setData(sortedList);
    });
    return () => {
      readStream();
    };
  }, [assetType]);

  useEffect(() => {
    if (assetType && asset) {
      const q = query(
        collectionRef,
        where('userId', '==', currentUser.uid),
        where('type', '==', assetType.toLowerCase()),
        where('iso3', '==', asset.toUpperCase()),
      );
      const readStream = onSnapshot(q, (querySnapshot) => {
        const list = [];
        querySnapshot.forEach((doc) => {
          list.push({ id: doc.id, purchaseDateIso: doc.data().purchaseDate.toDate().toDateString(), ...doc.data() });
        });
        list.forEach((item, i) => {
          setTotal((prev) => prev + item.price);
          setQuantity((prev) => prev + item.quantity);
          setTotalValue((prev) => prev + item.value);
        });
        setBought(list.length);
      });
      return () => {
        readStream();
      };
    }
  }, [asset]);

  return (
    <Container maxWidth='sm' sx={{
      marginTop: 4,
    }}>
      <Card>
        <CardContent>
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component='h1' variant='h5'>
              {asset ? asset.toUpperCase() : 'Choose your asset'}
            </Typography>
            <Box component='form' sx={{ mt: 2, width: '100%' }}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Type</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={assetType || ''}
                  label="Asset type"
                  name='assetType'
                  onChange={handleTypeChange}
                >
                  <MenuItem value='crypto' key='crypto'>Crypto</MenuItem>
                  <MenuItem value='gia' key='gia'>GIA</MenuItem>
                  <MenuItem value='isa' key='isa'>ISA</MenuItem>
                </Select>
              </FormControl>
            </Box>
            {assetType &&
              <Box component='form' sx={{ mt: 2, width: '100%' }}>
                <FormControl fullWidth>
                  <InputLabel id="demo-simple-select-label">Type</InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={asset || ''}
                    label="Asset"
                    name='asset'
                    onChange={handleChange}
                  >
                    {data.map((item) => (
                      <MenuItem value={item.iso3} key={item.id}>{item.name}</MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            }
            {asset &&
              <>
                <Paper sx={{ mt: 2, p: 2, width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} elevation={2}>
                  <span>
                    <Typography variant="h6">
                      <EventRepeatIcon sx={{mr: 2, mb: -0.5}}/>
                      <b>Bought</b>
                    </Typography>
                  </span>
                  <span>
                  <Paper elevation={8} sx={{p: 2}}>
                    <Typography variant="h5" sx={{textAlign: 'center'}}><b>{bought}</b></Typography>
                    <Divider />
                    <Typography variant="h6">times</Typography>
                  </Paper>
                  </span>
                </Paper>
                <Paper sx={{ mt: 2, p: 2, width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} elevation={2}>
                  <span>
                    <Typography variant="h6">
                      <EventRepeatIcon sx={{mr: 2, mb: -0.5}}/>
                      <b>Total</b>
                    </Typography>
                  </span>
                  <span>
                  <Paper elevation={8} sx={{p: 2}}>
                    <Typography variant="h5" sx={{textAlign: 'center'}}>{numberFormat(total)}</Typography>
                  </Paper>
                  </span>
                </Paper>
                <Paper sx={{ mt: 2, p: 2, width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} elevation={2}>
                  <span>
                    <Typography variant="h6">
                      <EventRepeatIcon sx={{mr: 2, mb: -0.5}}/>
                      <b>Average</b>
                    </Typography>
                  </span>
                  <span>
                  <Paper elevation={8} sx={{p: 2}}>
                    <Typography variant="h5" sx={{textAlign: 'center'}}>{numberFormat(totalValue/bought)}</Typography>
                    <Divider />
                    <Typography variant="h6" sx={{textAlign: 'center'}}>Per {asset}</Typography>
                  </Paper>
                  </span>
                </Paper>
                <Paper sx={{ mt: 2, p: 2, width: '100%', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }} elevation={2}>
                  <span>
                    <Typography variant="h6">
                      <EventRepeatIcon sx={{mr: 2, mb: -0.5}}/>
                      <b>Quantity</b>
                    </Typography>
                  </span>
                  <span>
                  <Paper elevation={8} sx={{p: 2}}>
                    <Typography variant="h5" sx={{textAlign: 'center'}}><b>{quantity.toFixed(2)}</b></Typography>
                    <Divider />
                    <Typography variant="h6" sx={{textAlign: 'center'}}>{asset}</Typography>
                  </Paper>
                  </span>
                </Paper>
              </>
            }
          </Box>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Details;