import * as React from 'react';
import { useNavigate } from "react-router-dom";
import  { 
  AppBar, 
  Avatar,
  Box,
  Toolbar,
  Typography,
  Menu,
  MenuItem,
  ListItemIcon,
  ListItemText,
  Divider,
} from '@mui/material';
import AssignmentIndIcon from '@mui/icons-material/AssignmentInd';
import LogoutIcon from '@mui/icons-material/Logout';
import ManageAccountsIcon from '@mui/icons-material/ManageAccounts';
import PaletteIcon from '@mui/icons-material/Palette';
import DashboardCustomizeIcon from '@mui/icons-material/DashboardCustomize';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FunctionsIcon from '@mui/icons-material/Functions';
import { useAuth } from '../../contexts/AuthContext';

export const Navbar = () => {
  const { logout, currentUser } = useAuth();
  const navigate = useNavigate();
  const [loading, setLoading] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleLogout = async () => {
    try {
      setLoading(true);
      handleClose();
      await logout();
      navigate('/');
    } catch (e) {
      console.error(e.message);
    }
  };
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleNavigate = (dest) => {
    handleClose();
    setTimeout(() => navigate(dest), 200);
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            iStats
          </Typography>
          {currentUser && 
          <>
            <Avatar onClick={handleClick} sx={{cursor: 'pointer'}} src={currentUser.photoURL} alt='Profile picture'/>
            <Menu
              id="basic-menu"
              anchorEl={anchorEl}
              open={open}
              onClose={handleClose}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'right',
              }}
              PaperProps={{
                elevation: 0,
                sx: {
                  overflow: 'visible',
                  filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
                  mt: 1.5,
                  '& .MuiAvatar-root': {
                    width: 32,
                    height: 32,
                    ml: -0.5,
                    mr: 1,
                  },
                  '&:before': {
                    content: '""',
                    display: 'block',
                    position: 'absolute',
                    top: 0,
                    right: 14,
                    width: 10,
                    height: 10,
                    bgcolor: 'background.paper',
                    transform: 'translateY(-50%) rotate(45deg)',
                    zIndex: 0,
                  },
                },
              }}
            >
              <MenuItem >
                <ListItemIcon>
                  <FavoriteIcon fontSize="medium" color="gray"/>
                </ListItemIcon>
                <ListItemText>
                  Welcome {currentUser.displayName?.split(' ')[0]}
                </ListItemText>
              </MenuItem>
              <Divider/>
              <MenuItem onClick={() => handleNavigate("/profile")}>
                <ListItemIcon>
                  <AssignmentIndIcon fontSize="medium" color="purple"/>
                </ListItemIcon>
                <ListItemText>
                  Profile
                </ListItemText>
              </MenuItem>
              <MenuItem onClick={() => handleNavigate("/account")}>
                <ListItemIcon>
                  <ManageAccountsIcon fontSize="medium" color="info"/>
                </ListItemIcon>
                <ListItemText>
                  Account
                </ListItemText>
              </MenuItem>
              <MenuItem onClick={() => handleNavigate("/theme")}>
                <ListItemIcon>
                  <PaletteIcon fontSize="medium" color="warning"/>
                </ListItemIcon>
                <ListItemText>
                  Theme
                </ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={() => handleNavigate("/dashboard")}>
                <ListItemIcon>
                  <DashboardCustomizeIcon fontSize="medium" color="blueGrey"/>
                </ListItemIcon>
                <ListItemText>
                  Dashboard
                </ListItemText>
              </MenuItem>
              <MenuItem onClick={() => handleNavigate("/dashboard/asset-details")}>
                <ListItemIcon>
                  <FunctionsIcon fontSize="medium" color='success' />
                </ListItemIcon>
                <ListItemText>
                  Sum & averages
                </ListItemText>
              </MenuItem>
              <Divider />
              <MenuItem onClick={handleLogout} disabled={loading}>
                <ListItemIcon>
                  <LogoutIcon fontSize="medium" color="error"/>
                </ListItemIcon>
                <ListItemText>
                  Sign out
                </ListItemText>
              </MenuItem>
            </Menu>
          </>
          }
        </Toolbar>
      </AppBar>
    </Box>
  );
};
