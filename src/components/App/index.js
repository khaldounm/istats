/* eslint-disable react-hooks/exhaustive-deps */
import  React, { lazy, Suspense } from 'react';
import {
  BrowserRouter as Router,
  Routes,
  Route,
} from 'react-router-dom';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import * as colors from '@mui/material/colors';
import {
  Login,
  ProtectedRoute,
  Navbar,
  Dashboard,
  Loading,
} from '../';
import { AuthProvider } from "../../contexts/AuthContext";

const NotFound = lazy(() => import('../NotFound'));
const Signup = lazy(() => import('../Forms/Signup'));
const Reset = lazy(() => import('../Forms/Reset'));
const Asset = lazy(() => import('../Forms/Asset'));
const Theme = lazy(() => import('../Theme'));
const Profile = lazy(() => import('../Forms/Profile'));
const Account = lazy(() => import('../Forms/Account'));
const Details = lazy(() => import('../Details'));

export const App = () => {

  const [mode, setMode] = React.useState(localStorage.getItem('theme') || 'light');
  const [color, setColor] = React.useState(colors[localStorage.getItem('color')] || colors.indigo);

  const { palette } = createTheme();

  let theme = createTheme({
    palette: {
      primary: color,
      secondary: colors.lightBlue,
      error: colors.red,
      warning: colors.orange,
      info: colors.blue,
      success: colors.green,
      mode,
      background: {
        default: mode === 'dark' ? '#0d1a2d' : '#ffffff',
        paper: mode === 'dark' ? '#081e3a' : '#ffffff',
      },
      purple: palette.augmentColor({ color: colors.purple }),
      lime: palette.augmentColor({ color: colors.lime }),
      pink: palette.augmentColor({ color: colors.pink }),
      blueGrey: palette.augmentColor({ color: colors.blueGrey }),
      red: palette.augmentColor({ color: colors.red }),
      green: palette.augmentColor({ color: colors.green }),
      orange: palette.augmentColor({ color: colors.orange }),
      teal: palette.augmentColor({ color: colors.teal }),
      blue: palette.augmentColor({ color: colors.blue }),
      grey: palette.augmentColor({ color: colors.grey }),
    },
    typography: {
      fontFamily: '"Quicksand", "Roboto", "Arial"',
    },
  });

  const handleThemeChange = (args) => {
    setMode(args);
  }

  const handleColorChange = (args) => {
    setColor(colors[args]);
  }

  return (
    <ThemeProvider theme={theme}>
      <AuthProvider>
        <Router>
          <CssBaseline />
          <Navbar />
          <Suspense fallback={<Loading />}>
          <Container maxWidth="xl">
            <Routes>
              <Route element={<ProtectedRoute />}>
                <Route path="/dashboard" >
                  <Route index element={<Dashboard />} />
                  <Route path="crypto" element={<Dashboard asset='crypto' />} />
                  <Route path="gia" element={<Dashboard asset='gia' />} />
                  <Route path="isa" element={<Dashboard asset='isa' />} />
                  <Route path="asset-details" element={<Details />} />
                  <Route path="asset" element={<Asset />} />
                </Route>
                <Route path="/profile" element={<Profile />} />
                <Route path="/account" element={<Account />} />
                <Route path="/theme" element={<Theme onThemeChange={handleThemeChange} onColorChange={handleColorChange} />} />
              </Route>
              <Route exact path='/' element={<Login />} />
              <Route exact path='/signup' element={<Signup />} />
              <Route exact path='/reset-password' element={<Reset />} />
              <Route path='*' element={<NotFound />} />
            </Routes>
          </Container>
          </Suspense>
        </Router>
      </AuthProvider>
    </ThemeProvider>
  );
}
