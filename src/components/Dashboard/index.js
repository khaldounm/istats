/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { 
  Button,
  Typography,
  Grid,
  Paper,
  BottomNavigation,
  BottomNavigationAction,
} from "@mui/material";
import {
  onSnapshot,
  collection,
  query,
  where,
} from 'firebase/firestore';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import AllInclusiveIcon from '@mui/icons-material/AllInclusive';
import AssuredWorkloadIcon from '@mui/icons-material/AssuredWorkload';
import CurrencyBitcoinIcon from '@mui/icons-material/CurrencyBitcoin';
import AutoGraphIcon from '@mui/icons-material/AutoGraph';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';

import { db } from '../../firebase';
import { useAuth } from '../../contexts/AuthContext';

const numberFormat = (value) =>
  new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency: 'GBP'
  }).format(value);

export const Dashboard = (props) => {
  const { asset } = props;
  const { currentUser } = useAuth();
  const [data, setData] = React.useState([]);
  const [portfolioVal, setPortfolioVal] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [value, setValue] = React.useState(asset || '');
  const navigate = useNavigate();
  const collectionRef = collection(db, 'investments');

  useEffect(() => {
    const q = asset ? query(
      collectionRef,
      where('userId', '==', currentUser.uid),
      where('type', '==', asset),
    ) : query(
      collectionRef,
      where('userId', '==', currentUser.uid),
    );
    setLoading(true);
    setPortfolioVal(0);
    const readStream = onSnapshot(q, (querySnapshot) => {
      const list = [];
      querySnapshot.forEach((doc) => {
        list.push({ id: doc.id, purchaseDateIso: doc.data().purchaseDate.toDate().toDateString(), ...doc.data() });
        setPortfolioVal((prevVal) => (prevVal + doc.data().price));
      });
      setData(list);
      setLoading(false);
    });
    return () => {
      readStream();
    };
  }, [asset]);

  const handleChange = (e, newValue) => {
    navigate(`/dashboard${newValue === '' ? newValue : '/' + newValue}`)
    setValue(newValue);
  }

  const columns = [
    {
      field: 'name',
      headerName: 'Asset',
      width: 110,
      editable: false,
    },
    {
      field: 'price',
      headerName: 'Price',
      type: 'number',
      width: 110,
      editable: false,
    },
    {
      field: 'quantity',
      headerName: 'Qty',
      type: 'number',
      width: 110,
      editable: false,
    },
    {
      field: 'value',
      headerName: 'Asset Price',
      type: 'number',
      width: 150,
      editable: false,
    },
    {
      field: 'purchaseDateIso',
      headerName: 'Purchase Date',
      width: 200,
      editable: false,
    },
  ];

  const rows = data;

  return (
    <>
    <Grid container>
      <Grid item xs={6}>
        <Button variant="contained" startIcon={<PlaylistAddIcon />} onClick={() => navigate('/dashboard/asset')} disabled={loading} sx={{ mt: 3, mb: 0 }}>
          Add new
        </Button>
      </Grid>
      <Grid item xs={6}>
        <Typography variant="h5" component="h3" sx={{ mt: 3, mb: 0, textAlign: 'right'}}>
          {numberFormat(portfolioVal)}
        </Typography>
      </Grid>
    </Grid>
    <Box sx={{ height: '66vh', width: '100%', mt: 3, mb: 0 }} >
      <DataGrid
        rows={rows}
        columns={columns}
        pageSize={100}
        rowsPerPageOptions={[100]}
        checkboxSelection
        disableSelectionOnClick
      />
    </Box>
    <Paper sx={{ position: 'fixed', bottom: 0, left: 0, right: 0 }} elevation={10}>
        <BottomNavigation
          value={value}
          onChange={handleChange}
        >
          <BottomNavigationAction label="All" icon={<AllInclusiveIcon />} value=''/>
          <BottomNavigationAction label="ISA" icon={<AssuredWorkloadIcon />} value='isa'/>
          <BottomNavigationAction label="CRYPTO" icon={<CurrencyBitcoinIcon />} value='crypto'/>
          <BottomNavigationAction label="GIA" icon={<AutoGraphIcon />} value='gia'/>
        </BottomNavigation>
      </Paper>
    </>
  );
}