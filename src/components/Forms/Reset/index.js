import * as React from 'react';
import { useNavigate } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Link from '@mui/material/Link';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import { Alert } from '@mui/material';

import { useAuth } from '../../../contexts/AuthContext';

const Reset = () => {
  const { resetPassword } = useAuth();
  const [error, setError] = React.useState('')
  const [loading, setLoading] = React.useState(false)
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (e.target.elements.email.value !== e.target.elements.cEmail.value) {
      return setError('Emails do not match');
    }
    try {
      setError('');
      setLoading(true);
      await resetPassword(e.target.elements.email.value);
      navigate('/');
    } catch (e) {
      setError(e.message.replace(/firebase: /gi, ''));
    }
    setLoading(false)
  };

  return (
    <Container maxWidth="sm" sx={{
      marginTop: 4,
    }}>
      <Card>
        <CardContent>
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Reset Password
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email" />
              <TextField
                margin="normal"
                required
                fullWidth
                id="confirm-email"
                label="Confirm Email Address"
                name="cEmail"
                autoComplete="cEmail" />
              {error && <Alert sx={{ mt: 3, mb: 2 }} severity="error">{error}</Alert>}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                disabled={loading}
                sx={{ mt: 3, mb: 2 }}
              >
                Send instructions
              </Button>
              <Grid container>
                <Grid item xs>
                  <Link onClick={() => navigate('/signup')} underline="hover" component="button">
                    {"Don't have an account? Sign up"}
                  </Link>
                </Grid>
                <Grid item>
                  <Link onClick={() => navigate('/')} underline="hover" component="button">
                    {"Got an account? Sign in"}
                  </Link>
                </Grid>
              </Grid>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Reset;