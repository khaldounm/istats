import React from 'react';
import { useNavigate } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { Alert } from '@mui/material';

import { db } from '../../../firebase';

import { useAuth } from '../../../contexts/AuthContext';

const Asset = () => {
  const { currentUser } = useAuth();
  const navigate = useNavigate();
  const [error, setError] = React.useState('');
  const [loading, setLoading] = React.useState(false);
  const [type, setType] = React.useState('crypto');

  const handleChange = (event) => {
    setType(event.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      db.collection("investments").add({
        createdAt: new Date(),
        iso3: e.target.elements.iso3.value,
        name: e.target.elements.name.value,
        price: parseFloat(e.target.elements.price.value),
        purchaseDate: new Date(e.target.elements.purchaseDate.value),
        quantity: parseFloat(e.target.elements.qty.value),
        type: e.target.elements.type.value,
        userId: currentUser.uid,
        value: parseFloat(e.target.elements.val.value),
      });
      navigate(`/dashboard/${e.target.elements.type.value}`);
    } catch (e) {
      setError(e.message.replace(/firebase: /gi, ''));
    }
    setLoading(false)
  };
  return (
    <Container maxWidth='sm' sx={{
      marginTop: 4,
    }}>
      <Card>
        <CardContent>
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component='h1' variant='h5'>
              Add asset
            </Typography>
            <Box component='form' onSubmit={handleSubmit} sx={{ mt: 1 }}>
              <TextField
                margin='normal'
                required
                fullWidth
                id='name'
                type='text'
                label='Asset name'
                name='name'
                autoFocus
                autoComplete='name' />
              <TextField
                margin='normal'
                required
                fullWidth
                name='iso3'
                label='Asset ISO3'
                type='text'
                id='iso3'
                autoComplete='iso3' />
              <TextField
                margin='normal'
                required
                fullWidth
                name='price'
                label='Price'
                type='number'
                id='price'
                inputProps={{
                  step: 0.01,
                }}
                autoComplete='price' />
              <TextField
                margin='normal'
                required
                fullWidth
                id='date'
                type='datetime-local'
                label='Purchase date'
                name='purchaseDate'
                InputLabelProps={{ shrink: true }}
                autoComplete='date' />
              <TextField
                margin='normal'
                required
                fullWidth
                id='qty'
                type='number'
                label='Quantity'
                name='qty'
                inputProps={{
                  step: 0.01,
                }}
                autoComplete='qty' />
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Type</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={type}
                  label="Type"
                  name='type'
                  onChange={handleChange}
                >
                  <MenuItem value='crypto'>Crypto</MenuItem>
                  <MenuItem value='gia'>GIA</MenuItem>
                  <MenuItem value='isa'>ISA</MenuItem>
                </Select>
              </FormControl>
              <TextField
                margin='normal'
                required
                fullWidth
                id='val'
                type='number'
                label='Asset value at date'
                name='val'
                inputProps={{
                  step: 0.01,
                }}
                autoComplete='val' />
              {error && <Alert sx={{ mt: 3, mb: 2 }} severity="error">{error}</Alert>}
              <Button
                type='submit'
                fullWidth
                variant='contained'
                disabled={loading}
                sx={{ mt: 2, mb: 2 }}
              >
                Submit
              </Button>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Asset;