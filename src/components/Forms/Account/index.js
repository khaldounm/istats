import * as React from 'react';
import { useNavigate } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import { Alert, Grid } from '@mui/material';

import { useAuth } from '../../../contexts/AuthContext';

const Account = () => {
  const { updateEmail, updatePassword, currentUser } = useAuth();
  const [error, setError] = React.useState('')
  const [loading, setLoading] = React.useState(false)
  const navigate = useNavigate();

  const handleEmailSubmit = async (e) => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await updateEmail(e.target.elements.email.value);
      navigate('/dashboard');
    } catch (e) {
      setError(e.message.replace(/firebase: /gi, ''));
    }
    setLoading(false)
  };
  const handlePasswordSubmit = async (e) => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await updatePassword(e.target.elements.password.value);
      navigate('/dashboard');
    } catch (e) {
      setError(e.message.replace(/firebase: /gi, ''));
    }
    setLoading(false)
  };
  return (
    <>
      <Container maxWidth="xl" sx={{
        mt: 4,
        mb: 4,
      }}>
        <Card>
          <CardContent>
            <Box
              sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Update account
              </Typography>
              <Box component="form" onSubmit={handleEmailSubmit} sx={{ mt: 1, width: '100%' }} >
                <Grid container gap={0} >
                  <Grid item xl={9} lg={9} sm={9} xs={12}>
                    <TextField
                      margin="normal"
                      required
                      fullWidth
                      id="email"
                      label="Email"
                      name="email"
                      type="email"
                      defaultValue={currentUser.email || ''}
                      autoFocus 
                      sx={{ maxWidth: '98%' }} />
                  </Grid>
                  <Grid item xl={3} lg={3} sm={3} xs={12}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      size='large'
                      disabled={loading}
                      sx={{ mt: 2, mb: 2, height: 56 }}
                    >
                      Update
                    </Button>
                  </Grid>
                </Grid>
              </Box>
              <Box component="form" onSubmit={handlePasswordSubmit} sx={{ mt: 1, width: '100%' }} >
                <Grid container gap={0} >
                  <Grid item xl={9} lg={9} sm={9} xs={12}>
                    <TextField
                      margin="normal"
                      required
                      fullWidth
                      id="password"
                      label="Password"
                      name="password"
                      type="password"
                      sx={{ maxWidth: '98%' }} />
                  </Grid>
                  <Grid item xl={3} lg={3} sm={3} xs={12}>
                    <Button
                      type="submit"
                      fullWidth
                      variant="contained"
                      size='large'
                      disabled={loading}
                      sx={{ mt: 2, mb: 2, height: 56 }}
                    >
                      Update
                    </Button>
                  </Grid>
                </Grid>
                {error && <Alert sx={{ mt: 3, mb: 2 }} severity="error">{error}</Alert>}
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Container>
    </>
  );
}

export default Account;