import * as React from 'react';
import { useNavigate } from "react-router-dom";
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Container from '@mui/material/Container';
import { Alert } from '@mui/material';

import { useAuth } from '../../../contexts/AuthContext';

const Profile = () => {
  const { manageProfile, currentUser } = useAuth();
  const [error, setError] = React.useState('')
  const [loading, setLoading] = React.useState(false)
  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      setError('');
      setLoading(true);
      await manageProfile(
        e.target.elements.name.value,
        e.target.elements.image.value,
      );
      navigate('/dashboard');
    } catch (e) {
      setError(e.message.replace(/firebase: /gi, ''));
    }
    setLoading(false)
  };
  return (
    <Container maxWidth="sm" sx={{
      marginTop: 4,
    }}>
      <Card>
        <CardContent>
          <Box
            sx={{
              marginTop: 8,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: 'primary.main' }}>
              <LockOutlinedIcon />
            </Avatar>
            <Typography component="h1" variant="h5">
              Update profile
            </Typography>
            <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
              <TextField
                margin="normal"
                required
                fullWidth
                id="name"
                label="Name"
                name="name"
                defaultValue={currentUser.displayName || ''}
                autoFocus />
              <TextField
                margin="normal"
                required
                fullWidth
                name="image"
                label="image"
                id="image"
                defaultValue={currentUser.photoURL || ''} />
              {error && <Alert sx={{ mt: 3, mb: 2 }} severity="error">{error}</Alert>}
              <Button
                type="submit"
                fullWidth
                variant="contained"
                disabled={loading}
                sx={{ mt: 3, mb: 2 }}
              >
                Update profile
              </Button>
            </Box>
          </Box>
        </CardContent>
      </Card>
    </Container>
  );
}

export default Profile;