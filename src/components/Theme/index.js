/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { 
  Typography,
  Grid,
  Divider,
  FormGroup,
  FormControlLabel,
  Switch,
  Container,
  Box,
  Card,
  CardContent,
} from "@mui/material";
import { styled } from '@mui/material/styles';

const MaterialUISwitch = styled(Switch)(({ theme }) => ({
  width: 62,
  height: 34,
  padding: 7,
  '& .MuiSwitch-switchBase': {
    margin: 1,
    padding: 0,
    transform: 'translateX(6px)',
    '&.Mui-checked': {
      color: '#fff',
      transform: 'translateX(22px)',
      '& .MuiSwitch-thumb:before': {
        backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
          '#fff',
        )}" d="M4.2 2.5l-.7 1.8-1.8.7 1.8.7.7 1.8.6-1.8L6.7 5l-1.9-.7-.6-1.8zm15 8.3a6.7 6.7 0 11-6.6-6.6 5.8 5.8 0 006.6 6.6z"/></svg>')`,
      },
      '& + .MuiSwitch-track': {
        opacity: 1,
        backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
      },
    },
  },
  '& .MuiSwitch-thumb': {
    backgroundColor: theme.palette.mode === 'dark' ? theme.palette.primary.dark : theme.palette.primary.main,
    width: 32,
    height: 32,
    '&:before': {
      content: "''",
      position: 'absolute',
      width: '100%',
      height: '100%',
      left: 0,
      top: 0,
      backgroundRepeat: 'no-repeat',
      backgroundPosition: 'center',
      backgroundImage: `url('data:image/svg+xml;utf8,<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 20 20"><path fill="${encodeURIComponent(
        '#fff',
      )}" d="M9.305 1.667V3.75h1.389V1.667h-1.39zm-4.707 1.95l-.982.982L5.09 6.072l.982-.982-1.473-1.473zm10.802 0L13.927 5.09l.982.982 1.473-1.473-.982-.982zM10 5.139a4.872 4.872 0 00-4.862 4.86A4.872 4.872 0 0010 14.862 4.872 4.872 0 0014.86 10 4.872 4.872 0 0010 5.139zm0 1.389A3.462 3.462 0 0113.471 10a3.462 3.462 0 01-3.473 3.472A3.462 3.462 0 016.527 10 3.462 3.462 0 0110 6.528zM1.665 9.305v1.39h2.083v-1.39H1.666zm14.583 0v1.39h2.084v-1.39h-2.084zM5.09 13.928L3.616 15.4l.982.982 1.473-1.473-.982-.982zm9.82 0l-.982.982 1.473 1.473.982-.982-1.473-1.473zM9.305 16.25v2.083h1.389V16.25h-1.39z"/></svg>')`,
    },
  },
  '& .MuiSwitch-track': {
    opacity: 1,
    backgroundColor: theme.palette.mode === 'dark' ? '#8796A5' : '#aab4be',
    borderRadius: 20 / 2,
  },
}));

const CardContentNoPadding = styled(CardContent)(`
  padding-top: 0;
  padding-bottom: 0;
  &:last-child {
    padding-bottom: 0;
  }
`);

const capitalise = s => s && s[0].toUpperCase() + s.slice(1)

const Theme = (props) => {
  const { onThemeChange, onColorChange } = props;
  const [theme, setTheme] = React.useState(localStorage.getItem('theme') || 'light');
  const [color, setColor] = React.useState(localStorage.getItem('color') || 'indigo');

  const toggleTheme = async () => {
    setTheme(theme === 'light' ? 'dark' : 'light');
  };

  const handleColor = (e) => {
    setColor(e.target.value);
  }

  React.useEffect(() => {
    onThemeChange(theme);
    localStorage.setItem('theme', theme);
  }, [theme]);

  React.useEffect(() => {
    onColorChange(color);
    localStorage.setItem('color', color);
  }, [color]);

  return (
    <>
    <Container maxWidth="xl" sx={{ mb: 3 }}>
        <Grid container>
          <Grid item xs={12} sx={{ mt: 3, mb: 3}}>
            <Typography variant="h5" component="h3">
              General
            </Typography>
          </Grid>
        </Grid>
        <Card>
          <CardContentNoPadding sx={{pt:0, pb: 0}}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
            <Grid container>
              <Grid item xs={6} sx={{ mt: 3, mb: 1}}>
                <Typography variant="h5" component="h3">
                  Theme
                </Typography>
              </Grid>
              <Grid item xs={6} sx={{display: 'flex', justifyContent: 'flex-end', mt: 2, mb: 2 }}>
                <FormGroup>
                  <FormControlLabel
                    control={<MaterialUISwitch sx={{ m: 1 }} onChange={toggleTheme} checked={theme === 'dark' }/>}
                    label={capitalise(theme)}
                  />
                </FormGroup>
              </Grid>
            </Grid>
          </Box>
        </CardContentNoPadding>
      </Card>
      <Grid container>
        <Grid item xs={12} sx={{ mt: 3, mb: 3}}>
          <Typography variant="h5" component="h3">
            Palette
          </Typography>
        </Grid>
      </Grid>
      <Card>
          <CardContentNoPadding sx={{ pt: 0, pb: 0 }}>
            <Box
              sx={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >
              <Grid container>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Red
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='red' checked={color === 'red'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Orange
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='orange' checked={color === 'orange'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Green
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='green' checked={color === 'green'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Teal
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='teal' checked={color === 'teal'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Blue
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='blue' checked={color === 'blue'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Lime
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='lime' checked={color === 'lime'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Blue Grey
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='blueGrey' checked={color === 'blueGrey'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Pink
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='pink' checked={color === 'pink'}/>
                </Grid>
                <Grid item xs={12}>
                  <Divider />
                </Grid>
                <Grid item xs={6} sx={{ mt: 3, mb: 2}}>
                  <Typography variant="h5" component="h3">
                    Purple
                  </Typography>
                </Grid>
                <Grid item xs={6} sx={{ mt: 2, mb: 2, textAlign: 'right' }}>
                  <Switch onChange={handleColor} value='purple' checked={color === 'purple'}/>
                </Grid>
              </Grid>
            </Box>
          </CardContentNoPadding>
        </Card>
      </Container>
    </>
  );
}

export default Theme;